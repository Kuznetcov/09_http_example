package test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;

public class HttpTest {

  public static void main(String[] args) throws URISyntaxException {
    // TODO Auto-generated method stub

    URI url = new URI(
        "https://yandex.ru/maps/api/search?lang=ru_RU&text=Ижевск,Репина,2&csrfToken=741e5205348c02a6588ee3d416bf059455a8bd51:1462963501816");

    HttpClient client = HttpClientBuilder.create().build();
    HttpGet request = new HttpGet(url);
    // add request header
    request.addHeader("Cookie", "yandexuid=2785989591462963501");
    HttpResponse response;
    try {
      response = client.execute(request);
      System.out.println("Response Code : " + response.getStatusLine().getStatusCode());

      BufferedReader rd =
          new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

      StringBuffer result = new StringBuffer();
      String line = "";
      while ((line = rd.readLine()) != null) {
        result.append(line);
      }

      JsonFactory f = new JsonFactory();
      JsonParser jp = f.createParser(result.toString());

      do {

        jp.nextToken();

      } while (jp.getCurrentName() != "InternalToponymInfo");

      if (jp.getCurrentName() == "InternalToponymInfo") {

        while (jp.getCurrentName() != "coordinates") {
          jp.nextToken();
        }

        System.out.print("Координаты: ");
        while (jp.getText() != "]") {

          jp.nextToken();
          System.out.print(jp.getText() + " ");

        }
      }

    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

  }

}
